﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Models
{
    public class DataOneModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public long Tok { get; set; }
        [Required]
        public string Jejson { get; set; }
        [Required]
        public DateTime Creation { get; set; }
        [Required]
        public byte Charr { get; set; }

    }
}
