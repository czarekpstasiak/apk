﻿using Apk;
using Apk.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApkXunitTests.Repos
{
    public static class DataOneRepMocker
    {
        public static IDataOneRepository<DataOneModel> GetInMemoryDataOneRepo(string name)
        {
            var options = new DbContextOptionsBuilder<DataContext>().UseInMemoryDatabase(databaseName: name).Options;
            
            DataContext dbcontext = new DataContext(options);
            dbcontext.FillDatabase();

            return new DataOneRepository(dbcontext);
        }
    }
}
