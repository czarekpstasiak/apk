﻿using Apk.LoggerFactory;
using Apk.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Repositories
{
    public class DataThreeRepository : IDataThreeRepository<DataThreeModel>
    {
        private readonly DataContext dbcontext;
        private ILogerBuilder logerBuilder;
        public DataThreeRepository(DataContext _context, ILogerBuilder _logerBuilder)
        {
            dbcontext = _context;
            logerBuilder = _logerBuilder;
        }


        public async Task Add(DataThreeModel entity)
        {
            await dbcontext.dataThree.AddAsync(entity);
            var log = logerBuilder.GenTemplate().AddMessage("New d3 model created. Description: " + entity.ObjName).AddRelatedData(entity).GetLog();
            await AddLogAndSaveContext(log);
        }

        public async Task Delete(DataThreeModel entity)
        {
            var history = dbcontext.loger.Where(lg => lg.RelatedData.Id == entity.Id);
            dbcontext.loger.RemoveRange(history);
            dbcontext.dataThree.Remove(entity);
            if (entity == null)
            {
                var nlog = logerBuilder.GenTemplate().AddMessage("Non existing D3 data delete request").AddRelatedData(null).GetLog();
                await AddLogAndSaveContext(nlog);
            }
            var log = logerBuilder.GenTemplate().AddMessage("D3 model deleted. Description: " + entity.ObjName).AddRelatedData(entity).GetLog();
            await AddLogAndSaveContext(log);
        }

        public async Task<DataThreeModel> Get(long id)
        {
            var datamodel = await dbcontext.dataThree.FirstOrDefaultAsync(a => a.Id == id);
            if (datamodel == null)
            {
                var nlog = logerBuilder.GenTemplate().AddMessage("Non existing D3 data requested").AddRelatedData(null).GetLog();
                await AddLogAndSaveContext(nlog);
                return null;
            }
            var log = logerBuilder.GenTemplate().AddMessage("D3 model requested. Description: " + datamodel.ObjName).AddRelatedData(datamodel).GetLog();
            await AddLogAndSaveContext(log);
            return datamodel;
        }

        public async Task<IEnumerable<DataThreeModel>> GetAll()
        {
            var log = logerBuilder.GenTemplate().AddMessage("All data requested").GetLog();
            await AddLogAndSaveContext(log);
            return await dbcontext.dataThree.ToListAsync();
        }

        public async Task Update(DataThreeModel toUpdate, DataThreeModel entity)
        {
            var datamodel = await Get(toUpdate.Id);
            datamodel.xCords = entity.xCords;
            datamodel.yCords = entity.yCords;
            datamodel.zCords = entity.zCords;
            await dbcontext.SaveChangesAsync();
        }

        private async Task AddLogAndSaveContext(LogerModel log)
        {
            await dbcontext.loger.AddAsync(log);
            await dbcontext.SaveChangesAsync();
        }
    }
}
