﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Apk.Models;
using Microsoft.EntityFrameworkCore;

namespace Apk
{
    public class DataOneRepository : IDataOneRepository<DataOneModel>
    {
        private readonly DataContext dbcontext;
        public DataOneRepository(DataContext _context)
        {
            dbcontext = _context;
        }

        public async Task Add(DataOneModel entity)
        {
            if (entity == null)
            {
                throw new NullReferenceException("Object cannot be null");
            }
            await dbcontext.dataOne.AddAsync(entity);
            await dbcontext.SaveChangesAsync();
        }

        public async Task Delete(DataOneModel dat)
        {
            if (dat == null)
            {
                return;
            }
            dbcontext.dataOne.Remove(dat);
            await dbcontext.SaveChangesAsync();
        }

        public async Task<DataOneModel> Get(long id)
        {
            var datamodel = await dbcontext.dataOne.FirstOrDefaultAsync(a => a.Id == id);
            return datamodel;
        }

        public async Task<IEnumerable<DataOneModel>> GetAll()
        {
            return await dbcontext.dataOne.ToListAsync();
        }

        public async Task Update(DataOneModel dat, DataOneModel entity)
        {
            if (ValidateModel(dat) == false || ValidateModel(entity) == false)
            {
                return;
            }
            var datamodel = await Get(dat.Id);
            datamodel.Tok = entity.Tok;
            datamodel.Jejson = entity.Jejson;
            datamodel.Charr = entity.Charr;
            await dbcontext.SaveChangesAsync();
        }

        private bool ValidateModel(DataOneModel model)
        {
            if (model == null)
            {
                return false;
            }

            if (model.Jejson == null)
            {
                return false;
            }

            return true;
        }
    }
}