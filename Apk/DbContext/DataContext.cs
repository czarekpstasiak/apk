﻿using Apk.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Apk
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<DataOneModel> dataOne { get; set; }
        public DbSet<DataTwoModel> dataTwo { get; set; }
        public DbSet<DataThreeModel> dataThree { get; set; }
        public DbSet<LogerModel> loger { get; set; }
    }
}