﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apk.Models;

namespace Apk.LoggerFactory
{
    public class LogerBuilder : ILogerBuilder
    {
        public LogerModel log;
        public LogerBuilder()
        {
            log = new LogerModel();
        }
        public LogerBuilder GenTemplate()
        {
            return new LogerBuilder();
        }
        public LogerBuilder AddMessage(string message)
        {
            log.LogMessage = message;
            return this;
        }

        public LogerBuilder AddRelatedData(DataThreeModel d3)
        {
            log.RelatedData = d3;
            return this;
        }

        public LogerModel GetLog()
        {
            log.LogDate = DateTime.Now;
            return log;
        }
        
    }
}
