﻿using Apk.Models;
using ApkXunitTests.Repos;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ApkXunitTests
{
    public class DataTwoRepositoryUnitTests
    {
        private DataTwoModel getDataTwoModelSample()
        {
            return new DataTwoModel(){ EmailHash = "sadad2e", PasswordHash = "asd21d1d", SingUpDate = DateTime.Now.Add(new TimeSpan(5, 10, 15, 0)), UserName="sampleUser"};
        }

        [Fact]
        public async void Add_ShouldAddObejctToDatabase()
        {
            //Arrange
            var repo = DataTwoRepMocker.GetInMemoryDataTwoRepo("B1");
            var obj = getDataTwoModelSample();

            //Act
            await repo.Add(obj);
            var result = await repo.Get(5);
            //Assert
            Assert.Equal(obj, result);

        }
    }
}
