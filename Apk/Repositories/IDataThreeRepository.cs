﻿using Apk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Repositories
{
    public interface IDataThreeRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> Get(long id);
        Task Add(TEntity entity);
        Task Update(DataThreeModel dat, TEntity entity);
        Task Delete(DataThreeModel dat);
    }
}
