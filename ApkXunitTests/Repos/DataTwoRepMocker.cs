﻿using Apk;
using Apk.Models;
using Apk.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApkXunitTests.Repos
{
    public static class DataTwoRepMocker
    {
        public static IDataTwoRepository<DataTwoModel> GetInMemoryDataTwoRepo(string name)
        {
            var options = new DbContextOptionsBuilder<DataContext>().UseInMemoryDatabase(databaseName: name).Options;

            DataContext dbcontext = new DataContext(options);
            dbcontext.FillDatabase();

            return new DataTwoRepository(dbcontext);
        }
    }
}
