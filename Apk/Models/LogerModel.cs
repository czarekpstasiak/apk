﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Models
{
    public class LogerModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string LogMessage { get; set; }
        [Required]
        public DateTime LogDate { get; set; }
        [ForeignKey("DataThreeModelKey")]
        public DataThreeModel RelatedData { get; set; }
    }
}
