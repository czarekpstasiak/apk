﻿using Apk;
using Apk.LoggerFactory;
using Apk.Models;
using Apk.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApkXunitTests.Repos
{
    public static class DataThreeRepMocker
    {
        public static IDataThreeRepository<DataThreeModel> GetInMemoryDataOneRepo(string name)
        {
            var options = new DbContextOptionsBuilder<DataContext>().UseInMemoryDatabase(databaseName: name).Options;

            DataContext dbcontext = new DataContext(options);
            dbcontext.FillDatabase();

            return new DataThreeRepository(dbcontext, new LogerBuilder());
        }
    }
}
