﻿using Apk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apk
{
    public interface IDataOneRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> Get(long id);
        Task Add(TEntity entity);
        Task Update(DataOneModel dat, TEntity entity);
        Task Delete(DataOneModel dat);
    }
}
