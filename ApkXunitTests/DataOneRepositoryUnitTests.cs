﻿using Apk;
using Apk.Models;
using ApkXunitTests.Repos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApkXunitTests
{
    public class DataOneRepositoryUnitTests
    {

        [Theory]
        [InlineData(1, 22313122)]
        [InlineData(3, 98891923)]
        [InlineData(2, 3321123)]
        public async Task Get_ShouldReturnRightObject(long id, long tok)
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B1");
            
            //Act
            var result = await repo.Get(id);

            //Assert
            Assert.Equal(tok, result.Tok);

        }

        [Fact]
        public async Task Get_ShouldReturnNull ()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B2");

            //Act
            var result = await repo.Get(5);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task Add_ShouldAddObjectToDatabase()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B3");
            var obj = new DataOneModel { Id = 5, Charr = 12, Creation = DateTime.Now, Jejson = "weae", Tok = 232 };

            //Act
            await repo.Add(obj);
            var result = await repo.Get(5);

            //Assert
            Assert.Equal(obj, result);
        }

        [Fact]
        public async Task Add_ShouldThrowNullReferenceException()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B3");

            //Assert
            //Act
            await Assert.ThrowsAsync<NullReferenceException>(async () => await repo.Add(null));
        }

        [Fact]
        public async Task GetAll_ShouldReturnRightAmountOfObjects()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B4");
            

            //Act
            var items = (List<DataOneModel>) await repo.GetAll();

            //Assert
            Assert.Equal(DataContextExt.dbObjectCount, items.Count);
        }

        [Fact]
        public async Task Update_ShouldProperlyUpdateObjectInDatabase()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B5");
            var toUpdate = new DataOneModel { Id = 3, Charr = 14, Creation = DateTime.Now, Jejson = "{qw : rr}", Tok = 98891923 };
            var update = new DataOneModel { Id = 21313, Charr = 231, Creation = DateTime.Now, Jejson = "blabla", Tok = 332211 };

            //Act
            await repo.Update(toUpdate, update);
            var result = await repo.Get(toUpdate.Id);

            //Assert
            Assert.Equal(update.Charr, result.Charr);
            Assert.Equal(update.Jejson, result.Jejson);
            Assert.Equal(update.Tok, result.Tok);
            Assert.NotEqual(update.Creation, result.Creation);
            Assert.NotEqual(update.Id, result.Id);
        }

        [Fact]
        public async Task Update_ShouldNotUpdateObjectInDatabase_NullProperty()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B6");
            var toUpdate = new DataOneModel { Id = 3, Charr = 14, Creation = DateTime.Now, Jejson = "{qw : rr}", Tok = 98891923 };
            var update = new DataOneModel { Id = 21313, Charr = 231, Creation = DateTime.Now, Jejson = null, Tok = 332211 };

            //Act
            await repo.Update(toUpdate, update);
            var result = await repo.Get(toUpdate.Id);

            //Assert
            Assert.NotEqual(update.Charr, result.Charr);
            Assert.NotEqual(update.Jejson, result.Jejson);
            Assert.NotEqual(update.Tok, result.Tok);
            Assert.NotEqual(update.Creation, result.Creation);
            Assert.NotEqual(update.Id, result.Id);
        }

        [Fact]
        public async Task Update_ShouldNotUpdateObjectInDatabase_NullReference()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B7");
            var toUpdate = new DataOneModel { Id = 3, Charr = 14, Creation = DateTime.Now, Jejson = "{qw : rr}", Tok = 98891923 };
            
            //Act
            await repo.Update(toUpdate, null);
            var result = await repo.Get(toUpdate.Id);

            //Assert
            Assert.Equal(toUpdate.Charr, result.Charr);
            Assert.Equal(toUpdate.Jejson, result.Jejson);
            Assert.Equal(toUpdate.Tok, result.Tok);
            Assert.NotEqual(toUpdate.Creation, result.Creation);
            Assert.Equal(toUpdate.Id, result.Id);
        }

        [Fact]
        public async Task Delete_ShouldDeleteObjectFromDatabase()
        {
            //Arrange
            var repo = DataOneRepMocker.GetInMemoryDataOneRepo("B8");
            var toDelete = await repo.Get(3);

            //Act
            await repo.Delete(toDelete);
            var deleted = await repo.Get(3);
            var otherObj = await repo.Get(1);
            var items = (List<DataOneModel>) await repo.GetAll();

            //Assert
            Assert.Null(deleted);
            Assert.NotNull(otherObj);
            Assert.Equal(3, items.Count);

        }

    }
}
