﻿using Apk.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Repositories
{
    public class DataTwoRepository : IDataTwoRepository<DataTwoModel>
    {
        private readonly DataContext dbcontext;
        public DataTwoRepository(DataContext _context)
        {
            dbcontext = _context;
        }

        public async Task Add(DataTwoModel entity)
        {
            await dbcontext.dataTwo.AddAsync(entity);
            await dbcontext.SaveChangesAsync();
        }

        public async Task Delete(DataTwoModel dat)
        {
            dbcontext.dataTwo.Remove(dat);
            await dbcontext.SaveChangesAsync();
        }

        public async Task<DataTwoModel> Get(long id)
        {
            var datamodel = await dbcontext.dataTwo.FirstOrDefaultAsync(a => a.Id == id);
            return datamodel;
        }

        public async Task<IEnumerable<DataTwoModel>> GetAll()
        {
            return await dbcontext.dataTwo.ToListAsync();
        }

        public async Task Update(DataTwoModel dat, DataTwoModel entity)
        {
            var datamodel = await Get(dat.Id);
            datamodel.UserName = entity.UserName;
            datamodel.PasswordHash = entity.PasswordHash;
            datamodel.EmailHash = entity.EmailHash;
            await dbcontext.SaveChangesAsync();
        }
    }
}
