﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Apk.Migrations
{
    public partial class namingD3Fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_loger_dataThree_DataOneModelKey",
                table: "loger");

            migrationBuilder.RenameColumn(
                name: "DataOneModelKey",
                table: "loger",
                newName: "DataThreeModelKey");

            migrationBuilder.RenameIndex(
                name: "IX_loger_DataOneModelKey",
                table: "loger",
                newName: "IX_loger_DataThreeModelKey");

            migrationBuilder.AddForeignKey(
                name: "FK_loger_dataThree_DataThreeModelKey",
                table: "loger",
                column: "DataThreeModelKey",
                principalTable: "dataThree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_loger_dataThree_DataThreeModelKey",
                table: "loger");

            migrationBuilder.RenameColumn(
                name: "DataThreeModelKey",
                table: "loger",
                newName: "DataOneModelKey");

            migrationBuilder.RenameIndex(
                name: "IX_loger_DataThreeModelKey",
                table: "loger",
                newName: "IX_loger_DataOneModelKey");

            migrationBuilder.AddForeignKey(
                name: "FK_loger_dataThree_DataOneModelKey",
                table: "loger",
                column: "DataOneModelKey",
                principalTable: "dataThree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
