﻿using Apk;
using Apk.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApkXunitTests.Repos
{
    public static class DataContextExt
    {
        public const int dbObjectCount = 4;
        public static void FillDatabase(this DataContext dbContext)
        {
            dbContext.dataOne.Add(new DataOneModel { Charr = 10, Creation = DateTime.Now, Jejson = "{qw : er}", Tok = 22313122 });
            dbContext.dataOne.Add(new DataOneModel { Charr = 12, Creation = DateTime.Now, Jejson = "{qw: re}", Tok = 3321123 });
            dbContext.dataOne.Add(new DataOneModel { Charr = 14, Creation = DateTime.Now, Jejson = "{qw : rr}", Tok = 98891923 });
            dbContext.dataOne.Add(new DataOneModel { Charr = 16, Creation = DateTime.Now, Jejson = "{qw : gg}", Tok = 57897123 });

            dbContext.dataTwo.Add(new DataTwoModel { EmailHash = "das89jda", PasswordHash = "sda12d1231", SingUpDate = DateTime.Now, UserName = "user1" });
            dbContext.dataTwo.Add(new DataTwoModel { EmailHash = "qwer32dw", PasswordHash = "9gsdf0e3ns", SingUpDate = DateTime.Now, UserName = "user2" });
            dbContext.dataTwo.Add(new DataTwoModel { EmailHash = "jgd7gjdf", PasswordHash = "nihniads92", SingUpDate = DateTime.Now, UserName = "user3" });
            dbContext.dataTwo.Add(new DataTwoModel { EmailHash = "zxc23vxc", PasswordHash = "vxcjisad98", SingUpDate = DateTime.Now, UserName = "user4" });

            dbContext.dataThree.Add(new DataThreeModel { ObjName = "Chair", xCords = 0.4, yCords = 0.8, zCords = 0.1  });
            dbContext.dataThree.Add(new DataThreeModel { ObjName = "Lamp", xCords = 0.2, yCords = 0.2, zCords = -0.4 });
            dbContext.dataThree.Add(new DataThreeModel { ObjName = "Ball", xCords = 0.6, yCords = 0.1, zCords = -0.9 });
            dbContext.dataThree.Add(new DataThreeModel { ObjName = "Wall", xCords = 0.7, yCords = 0.4, zCords = 1 });

            dbContext.SaveChanges();
        }
    }
}
