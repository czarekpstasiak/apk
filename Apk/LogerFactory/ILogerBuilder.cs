﻿using Apk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.LoggerFactory
{
    public interface ILogerBuilder
    {
        LogerBuilder GenTemplate();
        LogerBuilder AddMessage(string message);

        LogerBuilder AddRelatedData(DataThreeModel d3);

        LogerModel GetLog();
    }
}
