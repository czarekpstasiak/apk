﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Models
{
    public class DataThreeModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public double xCords { get; set; }
        [Required]
        public double yCords { get; set; }
        [Required]
        public double zCords { get; set; }
        [Required]
        public string ObjName { get; set; }

    }
}
