﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Apk.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dataOne",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tok = table.Column<long>(nullable: false),
                    Jejson = table.Column<string>(nullable: false),
                    Creation = table.Column<DateTime>(nullable: false),
                    Charr = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dataOne", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dataThree",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    xCords = table.Column<double>(nullable: false),
                    yCords = table.Column<double>(nullable: false),
                    zCords = table.Column<double>(nullable: false),
                    ObjName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dataThree", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dataTwo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmailHash = table.Column<string>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    SingUpDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dataTwo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "loger",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LogMessage = table.Column<string>(nullable: false),
                    LogDate = table.Column<DateTime>(nullable: false),
                    DataOneModelKey = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_loger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_loger_dataThree_DataOneModelKey",
                        column: x => x.DataOneModelKey,
                        principalTable: "dataThree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_loger_DataOneModelKey",
                table: "loger",
                column: "DataOneModelKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dataOne");

            migrationBuilder.DropTable(
                name: "dataTwo");

            migrationBuilder.DropTable(
                name: "loger");

            migrationBuilder.DropTable(
                name: "dataThree");
        }
    }
}
