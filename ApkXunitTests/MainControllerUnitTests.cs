﻿using Apk.Controllers;
using ApkXunitTests.Repos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApkXunitTests
{
    public class MainControllerUnitTests
    {

        [Fact]
        public async Task GetDataOneById_ValidId_ReturnsDataOneObject()
        {
            //Arrange
            var repoDataOne = DataOneRepMocker.GetInMemoryDataOneRepo("D1");
            var repoDataTwo = DataTwoRepMocker.GetInMemoryDataTwoRepo("D2");
            var repoDataThree = DataThreeRepMocker.GetInMemoryDataOneRepo("D3");
            var controller = new MainController(repoDataOne, repoDataTwo, repoDataThree);

            //Act
            //var result = await controller.GetDataOneById(2) as ObjectResult;

            //Assert
        }
    }
}
