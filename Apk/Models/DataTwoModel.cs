﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Apk.Models
{
    public class DataTwoModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string EmailHash { get; set; }
        [Required]
        public string PasswordHash { get; set; }
        public string UserName { get; set; }
        [Required]
        public DateTime SingUpDate { get; set; }

    }
}
