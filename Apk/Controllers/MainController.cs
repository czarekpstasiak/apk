﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apk.Models;
using Apk.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apk.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainController : ControllerBase
    {
        private readonly IDataOneRepository<DataOneModel> dataOneRep;
        private readonly IDataTwoRepository<DataTwoModel> dataTwoRep;
        private readonly IDataThreeRepository<DataThreeModel> dataThreeRep;
        public MainController(IDataOneRepository<DataOneModel> _dataOneRep, IDataTwoRepository<DataTwoModel> _dataTwoRep, IDataThreeRepository<DataThreeModel> _dataThreeRep)
        {
            dataOneRep = _dataOneRep;
            dataTwoRep = _dataTwoRep;
            dataThreeRep = _dataThreeRep;

        }

        [Route("getDataOneById/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetDataOneById(long id)
        {
            var result = await dataOneRep.Get(id);

            if (result == null)
            {
                return NotFound("Wrong id");
            }

            return Ok(result);
        }

        [Route("getAllDataTwo")]
        [HttpGet]
        public async Task<IActionResult> GetAllDataTwo()
        {
            IEnumerable<DataTwoModel> result = await dataTwoRep.GetAll();

            if (result.ToList().Count == 0)
            {
                return NotFound("No data in system");
            }

            return Ok(result.ToList());
        }

        [Route("deleteDataThreeById/{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteDataThreeById(long id)
        {
            var toDestroy = await dataThreeRep.Get(id);
            if (toDestroy == null)
            {
                return NotFound("Wrong id");
            }
            await dataThreeRep.Delete(toDestroy);
            return Ok("Object deleted");
        }
    }
}
